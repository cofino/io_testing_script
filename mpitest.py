#!/usr/bin/env python2.7
from mpi4py import MPI
from socket import gethostname

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
if rank == 0:
    print 'parent on host', gethostname()
else:
    print 'child on host', gethostname()
