#!/bin/bash

rundir="/apps/contrib/redis/testing_scripts/netcdf_testing"
testfile=/group_workspaces/jasmin/hiresgw/vol1/mj07/IO_testing_files/test_1d.nc
logfile=$rundir/read_results.csv
readcode=$rundir/readfile.py

echo 1$1
echo 2$2
echo 3$3
echo 4$4


if [ "$#" -eq 3 ]; then
    block_size=$3
    read_mode=$2

    echo $readcode $testfile $block_size $read_mode
    $readcode $testfile $read_mode $block_size >> $logfile
    fi

if [ "$#" = 4 ]; then
    rand_num=$4
    block_size=$3
    read_mode=$2

    echo $readcode $testfile $block_size $read_mode $randnum
    $readcode $testfile $read_mode $block_size $rand_num >> $logfile
    fi


echo 'Finished test'
