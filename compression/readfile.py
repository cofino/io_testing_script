#!/usr/bin/env python2.7
import sys
import numpy as np
from netCDF4 import Dataset
from time import time, clock


def readfile(fid,readtype,comp_type,var_read):
    
    nc = Dataset(fid,'r')
    var = nc.variables[var_read]
    dims = var.shape
    
    file_size = np.array(dims).prod()*8
    bytes_read = 0
    num_reads = 0
    wall_time = time()
    cpu_time = clock()
    if readtype == 'xy':
        for i in range(dims[0]/5):
            for j in range(dims[1]):
                temp = var[i,j,:,:]
                bytes_read += np.array(temp.shape).prod()*8
                bytes_per_read = np.array(temp.shape).prod()*8
                num_reads += 1
    elif readtype == 'xt':
        for i in range(dims[1]/5):
            for j in range(dims[2]):
                temp = var[:,i,j,:]
                bytes_read += np.array(temp.shape).prod()*8
                bytes_per_read = np.array(temp.shape).prod()*8
                num_reads += 1
    elif readtype == 'xyt':
        for i in range(dims[1]/5):
            temp = var[:,i,:,:]
            bytes_read += np.array(temp.shape).prod()*8
            bytes_per_read = np.array(temp.shape).prod()*8
            num_reads += 1
    else:
        raise ValueError('Read type "%s" not included' %readtype)


    cpu_time = clock()-cpu_time
    wall_time = time()-wall_time
    rate = bytes_read/wall_time
    print '%s,%s,%s,%s,%s,%s,%s,%s,%s' \
            % (file_size,bytes_read,bytes_per_read,num_reads,readtype,cpu_time,wall_time,rate/1000**2,comp_type)
   
def main():
    #dims = [4,4,4,4]
    #readtype = 'xt'
    #ind1b, ind1s, ind1e, ind2b, ind2s, ind2e = get_read_inds(readtype,dims)
    #list_ind = expand_ind(readtype, dims, ind1b, ind1s, ind1e, ind2b, ind2s, ind2e)
    #plot_test(dims, list_ind) # testing indices code

    fid = sys.argv[1]
    readtype = sys.argv[2]
    comp_type = sys.argv[3]
    var_read = sys.argv[4]
    readfile(fid,readtype,comp_type,var_read)
    # Read indices list
    


if __name__ == '__main__':
    main()
