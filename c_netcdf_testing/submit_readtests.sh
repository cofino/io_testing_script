#!/bin/bash

# Batch queue submission code for Jon's/Annette's read test code using C
rundir="/apps/contrib/redis/testing_scripts/c_netcdf_testing"
testfile=/group_workspaces/jasmin/hiresgw/vol1/mj07/IO_testing_files/test_1d.nc
logfile=$rundir/read_results.csv
readcode=$rundir/readfile

 if [ "$#" -eq 3 ]; then
    block_size=$3
    read_mode=$2

    echo $readcode $testfile $block_size $read_mode
    $readcode $testfile $block_size $read_mode | sed '5q;d' >> $logfile
    fi

if [ "$#" = 4 ]; then
    rand_num=$4
    block_size=$3
    read_mode=$2

    echo $readcode $testfile $block_size $read_mode $randnum
    $readcode $testfile $block_size $read_mode $rand_num| sed '5q;d' >> $logfile
    fi
