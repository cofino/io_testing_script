#!/bin/bash

dir="/apps/contrib/redis/testing_scripts/chunking_testing_4d"
logfile=$dir/read_results.csv
readcode=$dir/readfile.py


chunksize=$1

datadir="/group_workspaces/jasmin/hiresgw/vol1/mj07/IO_testing_files"

if [ "$1" == "A" ]; then
    testfile=$datadir/test_4d_chunkA.nc
fi
if [ "$1" == "B" ]; then
    testfile=$datadir/test_4d_chunkB.nc
    fi
if [ "$1" = "C" ]; then
    testfile=$datadir/test_4d_chunkC.nc
    fi
if [ "$1" = "D" ]; then
    testfile=$datadir/test_4d_chunkD.nc
    fi
if [ "$1" = "E" ]; then
    testfile=$datadir/test_4d_chunkE.nc
    fi
if [ "$1" = "F" ]; then
    testfile=$datadir/test_4d_chunkF.nc
    fi
if [ "$1" = "G" ]; then
    testfile=$datadir/test_4d_chunkG.nc
    fi


read_mode=$2

echo $chunksize $readcode $testfile $read_mode
$readcode $testfile $read_mode >> $logfile

echo 'Finished test'
